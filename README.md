# Usage
## Install
```bash
sudo apt install hugo
```

## Creating new post
```bash
hugo server -D  # Run local server
hugo new posts/title-of-the-new-post.md
```

