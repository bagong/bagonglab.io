---
title: "Run Gitlab CI Locally"
date: 2021-05-22T20:58:45+02:00
tags:
    - gitlab
    - CI
---

This post explains how to run `gitlab-ci.yml` configuration in your local
workstation.

<!--more-->

## Requirement
- Ubuntu 20.04
- Docker v20
- Git repo (Note: uncommitted changes (except for `gitlab-ci.yml`) will not be included by the runner)
- `gitlab-ci.yml` file

Minimal sample for `gitlab-ci.yml`:

```yaml
image: python:3.8-slim

before_script:
  - python --version
  - pip list
  - ls -lha

test:
  script:
    - echo "hello world"
```

## Steps
```bash
# Go into your repo root
cd my_repo

# Mount your repo in one terminal
docker run --rm --name gitlab-runner \
    -v $PWD:$PWD -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner

# Run the gitlab-runner on another terminal
docker exec -it -w $PWD gitlab-runner gitlab-runner exec docker test
```

The repo mounter container can be sent to the background, by adding `-d` flag.
But I always forget to kill it, and I do not like having unused process
running in the background, especially with the exposed Unix socket.

So I keep it running on the foreground, and `Ctrl-C` it when I am done.

## Reference
- https://stackoverflow.com/a/65920577
