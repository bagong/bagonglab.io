---
title: "Resize Ubuntu LVM in TrueNAS Bhyve"
date: 2021-11-21T20:07:04+01:00
tags:
  - TrueNAS
  - ubuntu
---

This post summarizes the steps required to increase the partition size of Ubuntu
20.04 Server running as VM, with LVM volume management.

<!--more-->

## Problem
Ubuntu 20.4 VM has no space left. It uses LVM.

## Context
TrueNAS 12 with bhyve, Ubuntu 20.04 Server installed as VM with all default
settings.

## Steps
1. Resize VM's `zvol` to desired size (e.g. 20G) at TrueNAS Web UI.

2. Reboot VM, see if disk (e.g. `/dev/sda`) is resized with `lsblk`:
```bash
$ lsblk
NAME                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
loop0                       7:0    0 55.4M  1 loop /snap/core18/2128
loop1                       7:1    0 61.9M  1 loop /snap/core20/1242
loop2                       7:2    0 70.3M  1 loop /snap/lxd/21029
loop3                       7:3    0 55.5M  1 loop /snap/core18/2253
loop4                       7:4    0 32.5M  1 loop /snap/snapd/13640
loop5                       7:5    0 32.3M  1 loop /snap/snapd/12704
loop6                       7:6    0 67.2M  1 loop /snap/lxd/21835
sda                         8:0    0   20G  0 disk 
├─sda1                      8:1    0  512M  0 part /boot/efi
├─sda2                      8:2    0    1G  0 part /boot
└─sda3                      8:3    0  8.5G  0 part 
  └─ubuntu--vg-ubuntu--lv 253:0    0  8.5G  0 lvm  /
```

3. Extend `/dev/sda3` using `cfdisk`, don't forget to call `write` before
   quitting. Check `lsblk` and make sure `/dev/sda3` is increased in size.

4. Extend the physical volume (pv):
```bash
sudo pvresize /dev/sda3

## Sample output:
#  Physical volume "/dev/sda3" changed
#  1 physical volume(s) resized or updated / 0 physical volume(s) not resized
```

5. Extend the logical volume (lv):
```bash
# find it first with:
sudo lvdisplay | grep "LV Path"

## Sample output:
# LV Path         /dev/ubuntu-vg/ubuntu-lv

# now resize
sudo -E lvresize -l +100%FREE /dev/ubuntu-vg/ubuntu-lv

## Sample output:
#   Size of logical volume ubuntu-vg/ubuntu-lv changed from <8.50 GiB (2175 extents) to <18.50 GiB (4735 extents).
#   Logical volume ubuntu-vg/ubuntu-lv successfully resized.
```

6. `lsblk` should output the same size for `/dev/sda3` and the LVM logical
   volume. `sudo lvdisplay` should also report the increased size. But `df -h`
   still reports the old size on the root (`/`) mount point

7. Increase the partition on top of the logical volume
```bash
sudo resize2fs /dev/ubuntu-vg/ubuntu-lv

## Sample output:
# resize2fs 1.45.5 (07-Jan-2020)
# Filesystem at /dev/ubuntu-vg/ubuntu-lv is mounted on /; on-line resizing required
# old_desc_blocks = 2, new_desc_blocks = 3
# The filesystem on /dev/ubuntu-vg/ubuntu-lv is now 4848640 (4k) blocks long.
```

8. Now `df -h` should report increased size on the root (`/`) mount point.


## Sources
- https://carleton.ca/scs/2019/extend-lvm-disk-space/#extend-lvm-manual
- https://fabianlee.org/2021/02/19/ubuntu-extending-capacity-of-an-lvm-volume-group-using-an-existing-or-new-disk/
