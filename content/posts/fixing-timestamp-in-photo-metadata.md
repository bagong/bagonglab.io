---
title: "Batch Fix Timestamp in Photo Metadata"
date: 2021-01-18T22:56:14+01:00
draft: false
tags:
    - organization
---

### Problem
Social Media, in this case WhatsApp, strips metadata from media (photos and
videos) when user uploads it to their platform.
This is unsuitable for local photo archiving and organisation that sorts media
by the date.
<!--more-->

### Solution Overview
Because WhatsApp encodes the timestamp of upload on the filename, I could easily
convert this into the timestamp EXIF Metadata (field `DateTime`,
`DateTimeOriginal`, and `DateTimeDigitized`).

Technically, the upload timestamp and the EXIF timestamp metadata are two
different things, but in my case, considering how we use Whatsapp, this should
be a good approximation to the original timestamp.


### Solution

Install Phil Harvey's exiftool on Ubuntu 20.04:
```bash
sudo apt install libimage-exiftool-perl
```

**WARNING**: do this part on a fresh copy of your photos folder to play around!

This only works when filename follows `YYYY:mm:dd`, see [^1]. WhatsApp Media
file has this format: `IMG-YYYYmmdd-WAXXXX.jpg`.

```bash
exiftool "-alldates<filename" \
    -xmp:description="Photos obtained from WhatsApp, timestamp is upload time" \
    -Keywords+=WhatsApp -Keywords+=exiftool \
    -overwrite_original \  # to discard original files
    -r \  # recursive
    my_photos_folder
```

With this relatively simple command, `exiftool` parses the year, month, and date
correctly, but it parses the `XXXXX` part into minute. To properly ignore this
field, you have to dig deeper into the documentation and wrangle with `regex`.
Ain't nobody got time for that!


### To Do
- Check if this applies to videos

[^1]: https://exiftool.org/faq.html#Q5
[^2]: https://photo.stackexchange.com/a/105063
