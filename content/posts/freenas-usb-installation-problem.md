---
title: "FreeNAS 12.0 USB Installation Problem"
date: 2021-01-28T23:03:59+01:00
draft: false
tags:
    - FreeNAS
    - TrueNAS
---
### Problem
FreeNAS 12 (or TrueNAS 12.0 CORE) installation from USB flash drive to USB flash
drive as boot device fails with checksum error message.
<!--more-->
```
--snip--
FreenasOS.Exception.ChecksumFailException: %base-os-12.0-U1.1-95xxxxxxxxxxx.tgz has invalid checksum
The FreeNAS installation on ada0 has failed. Press enter to continue..
```

### Context
- Homebrew NAS system with ASRock J4105 Celeron on Mini-ITX Chassis
- 8GB Non-ECC RAM
- UEFI boot
- Checksum of installer ISO matches
- USB installation media created with
```bash
sudo dd if=/TrueNAS.iso of=/dev/sdb bs=4096 status=progress
sudo sync
```

### Solution
Unplug any USB Front Panel connector on the motherboard. Weird, I know.

#### References:
- [TrueNAS Community Forum Thread #1](https://www.truenas.com/community/threads/freenas-install-failed-checksum-error.74959/)
    - Guy gave up on USB installation, uses CD instead and it worked.
- [TrueNAS Community Forum Thread #2](https://www.truenas.com/community/threads/grub-error-checksum-verification-failed-after-update.27872/)
    - Botched USB flash drive was at fault, replacing it was the solution.
- [Reddit Thread
  /r/freenas](https://www.reddit.com/r/freenas/comments/f0wym3/failed_checksum_on_usb_install/)
    - Dude uplugged USB 3.0 header on the motherboard.

### Background
I wanted to play around with FreeNAS on my Mini-ITX box with ASRock J4105
motherboard inside. I heard good things about it --- the ZFS backend should provide
a robust backup/snapshot and disaster recovery, combined with intuitive GUI to
ease the learning curve. USB flash drive can be used as boot drive, and even
better: there is an option to have a mirrored boot drive, just in case one boot
drive goes bust. Furthermore, if both boot device are busted, putting the NAS
back into operation *should* be as simple as reinstalling FreeNAS on a new boot
device, and all settings should be automatically imported from the data disk
(TODO: gotta check how simple it works).

I think one question that is often forgotten in such DIY NAS setup, is not only
how robust it is under normal operating circumstances, but also **how easy** it
is to restore operation, once unforeseen events happened. Data can be
accidentally deleted, hard drive failing, boot drive corrupted, etc. Panicking
while browsing forum posts, looking for commands to type deep in page 42 won't
be a nice experience. The NAS software/OS has to include such failures as probable
use case, and has to have a built-in solution for that.

The nearest contender for a DIY NAS is Openmediavault. Although I feel more
comfortable with Debian-based systems, OMV basis needs more configuration steps
to get it up and running. I could do that if I had time, but since I am lazy, I
went to something that has a good default settings, something that *"Just
Works" (TM)*.

My only concern now is how bad is the lack of Non-ECC RAM for ZFS.
