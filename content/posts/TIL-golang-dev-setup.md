---
title: "Golang Dev Setup with private git repo"
date: 2021-02-26T18:13:54+01:00
draft: false
tags:
    - golang
---

## Problem
Go module is located in a private repo. Local Go development setup can't
download said module.

<!--more-->

## Context
- VCS in use: Gitlab.com
- Public SSH Key is added to Gitlab
- Gitlab password is randomized and stored in password manager --- password
  authentication in terminal is very uncomfortable

## Solution
### Global (User) Git config
Set git to always use SSH to pull, instead of https:
```bash
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
```
Do not forget the colon on `git@gitlab.com:` and the trailing slash on
`https://gitlab.com/`. This is one of the differences between the URL of
your git repo between HTTPS and SSH.

Make sure it is set properly in `~/.gitconfig`:
```yaml
# --snip--
[url "git@gitlab.com:"]
        insteadOf = https://gitlab.com/
```


### Go Development Setup
Let Go know that certain URLs are private:
```bash
go env -w GOPRIVATE=gitlab.com/your_username
```
To test:
```bash
$ go env | grep GOPRIVATE
GOPRIVATE="gitlab.com/bagong"
```

Failure to set `GOPRIVATE` or mismatch in values would be indicated by following
error message when trying to fetch Go module:
```
go: finding gitlab.com/bagong/greetings latest
go: downloading gitlab.com/bagong/greetings v0.0.0-20210226165958-c183cfe6723e
verifying gitlab.com/bagong/greetings@v0.0.0-20210226165958-c183cfe6723e: gitlab.com/bagong/greetings@
v0.0.0-20210226165958-c183cfe6723e: reading https://sum.golang.org/lookup/gitlab.com/bagong/greetings@
v0.0.0-20210226165958-c183cfe6723e: 410 Gone
```
